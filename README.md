## Descrição do Projeto

Este projeto é o front-end de uma calculadora de tinta desenvolvida utilizando React.js e Next.js. O objetivo deste repositório é fornecer uma interface amigável para que os usuários possam inserir os dados das paredes que desejam pintar, incluindo a altura,  a largura, a quantidade de portas e as janelas. Os dados são enviados para um back-end que realiza os cálculos necessários e retorna a quantidade de tinta necessária e a quantidade de latas sugeridas.

O back-end esta rodando na porta 5500 e para fazer a conexão com o back-end resolve usar o axios nesse projeto.

## Dependências

Para iniciar o projeto, é necessário instalar algumas dependências. No console, execute um dos seguintes comandos:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

## Estrutura do Projeto

O front-end foi configurado para coletar dados de quatro paredes, permitindo que o usuário insira a altura, largura, número de portas e janelas para cada parede. Uma vez inseridos os dados, o front-end envia uma requisição POST ao servidor com os dados coletados e exibe os resultados retornados, que incluem a área total a ser pintada, a quantidade de tinta necessária e a quantidade e tamanhos das latas de tinta recomendadas.

## Componentes

Form.js: Este componente renderiza o formulário que permite ao usuário inserir os dados das paredes. Ele também lida com a lógica de envio dos dados ao servidor e exibição dos resultados.
Exemplo de Uso
A interface do usuário é composta por quatro seções, uma para cada parede. Cada seção inclui campos para inserir a altura, largura, número de portas e janelas. Após preencher todos os campos, o usuário pode clicar no botão "Calcular" para enviar os dados ao back-end.

## Funcionamento

- Coleta de Dados: O usuário insere os dados de cada parede no formulário.

- Envio de Dados: Ao clicar no botão "Calcular", os dados são enviados para o back-end via uma requisição POST.

- Recebimento de Resultados: O back-end processa os dados e retorna a quantidade de tinta necessária e a sugestão de latas.

- Exibição dos Resultados: Os resultados são exibidos na interface do usuário, mostrando a área total a ser pintada, a quantidade de tinta necessária e a sugestão de latas.

- Este projeto oferece uma maneira simples e eficiente para calcular a quantidade de tinta necessária para pintar uma sala, facilitando o planejamento e a compra de materiais.
